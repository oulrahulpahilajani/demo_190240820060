const express=require('express');
const mysql=require('mysql');
const router=express.Router();

function dbconnection()
{
    const connection=mysql.createConnection({
        host:'localhost',
        user:'root',
        password:'manager',
        database:'mean'
    });

    connection.connect();
    return connection;
}

router.get('/movies', (request, response) => {
    const connection=dbconnection();
    const statement=`select * from movies`;
    connection.query(statement,(error,result)=>{
        console.log(result);
        connection.end();
        response.send(result);
    })
});

module.exports=router;