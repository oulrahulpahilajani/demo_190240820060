const express=require('express');
const server=express();
const route=require('./product');
server.use(route);
server.listen(4000, () => {
    console.log('App listening on port 4000!');
});